====== J.U.L.I.A Among the Stars ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-julia-among-the-stars.sh|play-julia-among-the-stars.sh]] (updated on 30/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 18/03/2018)
  * target:
    * [[https://www.gog.com/game/julia_among_the_stars|gog_j_u_l_i_a_among_the_stars_2.0.0.1.sh]] (MD5: 58becebfaf5a3705fe3f34d5531298d3)
    * [[https://www.dotslashplay.it/ressources/julia-among-the-stars/|julia-among-the-stars_icons.tar.gz]] (MD5: 8e9e8ec585123eb3b6e5d31723b7909c)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/julia-among-the-stars/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/julia-among-the-stars/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
gog_j_u_l_i_a_among_the_stars_2.0.0.1.sh
julia-among-the-stars_icons.tar.gz
libplayit2.sh
play-julia-among-the-stars.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-julia-among-the-stars.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
