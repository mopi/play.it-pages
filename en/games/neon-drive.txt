====== Neon Drive ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-neon-drive.sh|play-neon-drive.sh]] (updated on 2/09/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 18/03/2018)
  * target:
    * [[https://www.humblebundle.com/store/neon-drive|NeonDrive_V1.5_Linux.zip]] (MD5: 1fcbd5dc69cc08899b792b9f4c0d7075)
  * dependencies:
    * Arch Linux:
      * unzip
    * Debian:
      * fakeroot
      * unzip

[[https://www.dotslashplay.it/images/games/neon-drive/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/neon-drive/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
$ yaourt -S unzip
</code>
    - Debian:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
NeonDrive_V1.5_Linux.zip
play-neon-drive.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-neon-drive.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
