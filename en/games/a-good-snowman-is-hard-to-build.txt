====== A Good Snowman is Hard to Build ======

===== Description =====

[[https://www.dotslashplay.it/images/games/a-good-snowman-is-hard-to-build/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/a-good-snowman-is-hard-to-build/thumbnail.jpg?nocache }}]]

**A Good Snowman is Hard to Build** is a puzzle game about being a monster and making snowmen.

  * age: all
  * category: puzzle, brainstorm
  * official url: [[http://agoodsnowman.com/|]]
  * release date: 2015

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-a-good-snowman-is-hard-to-build.sh|play-a-good-snowman-is-hard-to-build.sh]] (updated on 30/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 18/03/2018)
  * targets:
    * [[https://www.humblebundle.com/store/a-good-snowman-is-hard-to-build|snowman-linux-1.0.8.tar.gz]] (MD5: 4461dfdcaba9e8793e3044b458b0e301)
    * [[https://www.dotslashplay.it/ressources/a-good-snowman-is-hard-to-build/|a-good-snowman-is-hard-to-build_icons.tar.gz]] (MD5: 8d595a7758ae8cd6dbc441ab79579fb4)
  * dependencies:
    * Debian:
      * fakeroot

==== Usage ====

  - Install the scripts dependencies:
    - Debian:<code>
# apt-get install fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
a-good-snowman-is-hard-to-build_icons.tar.gz
libplayit2.sh
play-a-good-snowman-is-hard-to-build.sh
snowman-linux-1.0.8.tar.gz
</code>
  - Start the building process from this directory:<code>
$ sh ./play-a-good-snowman-is-hard-to-build.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
