====== Pid ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-pid.sh|play-pid.sh]] (updated on 18/02/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 18/03/2018)
  * target:
    * [[https://www.gog.com/game/pid|setup_pid_gog-1_(18421).exe]] (MD5: d6cd6899df3b2ad13071f58d0362ab3a)
  * dependencies:
    * Arch Linux:
      * icoutils
      * innoextract
    * Debian:
      * fakeroot
      * icoutils
      * innoextract

[[https://www.dotslashplay.it/images/games/pid/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/pid/thumbnail.jpg?nocache }}]]

<note>The game installed via these scripts will use [[https://www.winehq.org/|WINE]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
$ yaourt icoutils innoextract
</code>
    - Debian:<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-pid.sh
setup_pid_gog-1_(18421).exe
</code>
  - Start the building process from this directory:<code>
$ sh ./play-pid.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
