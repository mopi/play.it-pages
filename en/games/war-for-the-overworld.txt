====== War for the Overworld ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-war-for-the-overworld.sh|play-war-for-the-overworld.sh]] (updated on 16/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 18/03/2018)
  * targets:
    * GOG:
      * [[https://www.gog.com/game/war_for_the_overworld|war_for_the_overworld_en_1_6_66_16455.sh]] (MD5: 3317bba3d2ec7dc5715f0d44e6cb70c1)
      * optional: [[https://www.gog.com/game/war_for_the_overworld_underlord_edition_upgrade|gog_war_for_the_overworld_underlord_edition_upgrade_dlc_2.0.0.1.sh]] (MD5: 635912eed200d45d8907ab1fb4cc53a4)
    * Humble Bundle:
      * War_for_the_Overworld_v1.5.2_-_Linux_x64.zip (MD5: bedee8b966767cf42c55c6b883e3127c) - **[[#obsolete_humble_version_and_free_gog_version|/!\ This version is no longer kept up-to-date]]**
  * dependencies:
    * Arch Linux:
      * GOG:
        * libarchive
      * Humble Bundle:
        * unzip
    * Debian:
      * GOG:
        * bsdtar
        * fakeroot
      * Humble Bundle:
        * fakeroot
        * unzip

[[https://www.dotslashplay.it/images/games/war-for-the-overworld/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/war-for-the-overworld/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:
      - GOG:<code>
# pacman -S libarchive
</code>
      - Humble Bundle:<code>
# pacman -S unzip
</code>
    - Debian:
      - GOG:<code>
# apt-get install bsdtar fakeroot
</code>
      - Humble Bundle:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory the scripts and archive:
    - GOG:<code>
$ ls
</code><code>
libplayit2.sh
play-war-for-the-overworld.sh
war_for_the_overworld_en_1_6_66_16455.sh
</code>
    - Humble Bundle:<code>
$ ls
</code><code>
libplayit2.sh
play-war-for-the-overworld.sh
War_for_the_Overworld_v1.5.2_-_Linux_x64.zip
</code>
  - Start the building process from this directory:<code>
$ sh ./play-war-for-the-overworld.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Obsolete Humble version and free GOG version ====

<note>The DRM-free version of War for the Overworld previously sold on Humble Bundle will no longer be updated.</note>
All owners of this version can ask for a free GOG version, this one is kept up-to-date and is the one and only official DRM-free version. Just send an e-mail to support@brightrockgames.com with your order number, and Brightrock Games will come back tou you with a gift code for War for the Overworld that you can redeem on GOG.


More infos about the GOG DRM-free version can be found on the following page:\\
[[https://brightrockgames.userecho.com/topics/2302-gog-drm-free-build-faq/|GOG & DRM-Free Build FAQ]]
