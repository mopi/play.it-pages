====== Gomo ======

===== Description =====

[[https://www.dotslashplay.it/images/games/gomo/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/gomo/thumbnail.jpg?nocache }}]]

**Gomo**: Help Gomo to save his dog and best friend Dingo on his journey.

  * age: 7y+
  * category: Adventure, Point’n Click, Fantasy
  * official url: [[http://www.playgomo.com/|]]
  * release date: 2013

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-gomo.sh|play-gomo.sh]] (updated on 6/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 18/03/2018)
  * target:
    * [[https://www.gog.com/game/gomo|setup_gomo_2.1.0.4.exe]] (MD5: 5ee422dff6f00976e170296103dd29e6)
  * dependencies:
    * Arch Linux:
      * icoutils
      * innoextract
    * Debian:
      * fakeroot
      * icoutils
      * innoextract

<note>The game installed via these scripts will use [[https://www.winehq.org/|WINE]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
$ yaourt icoutils innoextract
</code>
    - Debian:<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-gomo.sh
setup_gomo_2.1.0.4.exe
</code>
  - Start the building process from this directory:<code>
$ sh ./play-gomo.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
