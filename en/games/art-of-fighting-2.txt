====== Art of Fighting 2 ======

===== Description =====

[[https://www.dotslashplay.it/images/games/art-of-fighting-2/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/art-of-fighting-2/thumbnail.jpg?nocache }}]]

**Art of Fighting 2**: 2D combat video game

  * category: fight
  * release date: 2008

===== Informations =====

//version sold on Humble Bundle//

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-art-of-fighting-2_humble.sh|play-art-of-fighting-2_humble.sh]] (updated on 9/04/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (updated on 18/03/2018)
  * target:
    * ArtOfFighting2.sh (MD5: 835e36b56238c637ab39cac5e23e7b25)
  * dependencies:
    * fakeroot
    * unzip

==== Usage ====

  - Install the scripts dependencies:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory the scripts and installer:<code>
$ ls
</code><code>
ArtOfFighting2.sh
play-anything.sh
play-art-of-fighting-2_humble.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-art-of-fighting-2_humble.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Advanced usage ====

This script accept several options to set its behaviour. Follow the links to more details on how to use them.

  * [[:en:common:advanced-options#checksum|checksum]]
  * [[:en:common:advanced-options#compression|compression]]
  * [[:en:common:advanced-options#help|help]]
  * [[:en:common:advanced-options#prefix|prefix]]

==== Links ====

  * [[https://en.wikipedia.org/wiki/Art_of_Fighting|Art of Fighting on Wikipedia]]
