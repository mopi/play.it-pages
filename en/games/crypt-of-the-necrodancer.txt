====== Crypt of the NecroDancer ======

===== Description =====

[[https://www.dotslashplay.it/images/games/crypt-of-the-necrodancer/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/crypt-of-the-necrodancer/thumbnail.jpg?nocache }}]]

**Crypt of the NecroDancer** is an hardcore rhythm roguelike. Move in rhythm and hit your enemies with music!

  * category: RPG, roguelike, turn-based
  * official url: [[http://necrodancer.com/|]]
  * release date: 2015

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-crypt-of-the-necrodancer.sh|play-crypt-of-the-necrodancer.sh]] (updated on 30/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 18/03/2018)
  * target:
    * [[https://www.gog.com/game/crypt_of_the_necrodancer|crypt_of_the_necrodancer_en_1_29_14917.sh]] (MD5: 70d3e29a2a48901d02541d8b1c6326ba)
    * [[https://www.dotslashplay.it/ressources/crypt-of-the-necrodancer/|crypt-of-the-necrodancer_icons.tar.gz]] (MD5: 04d2bb19adc13dbadce6161bd92bf59a)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
crypt_of_the_necrodancer_en_1_29_14917.sh
crypt-of-the-necrodancer_icons.tar.gz
libplayit2.sh
play-crypt-of-the-necrodancer.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-crypt-of-the-necrodancer.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
