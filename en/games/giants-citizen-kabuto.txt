====== Giants: Citizen Kabuto ======

===== Description =====

[[https://www.dotslashplay.it/images/games/giants-citizen-kabuto/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/giants-citizen-kabuto/thumbnail.jpg?nocache }}]]

**Giants: Citizen Kabuto**: Immerse yourself in the vast world where Meccaryns, Marines, and Kabuto fight for possession of the island.

  * category: Action, Adventure, RPG
  * release date: 2000

===== Informations =====

//version sold on GOG//

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-giants-citizen-kabuto_gog-2.1.0.4.sh|play-giants-citizen-kabuto_gog-2.1.0.4.sh]] (updated on 21/05/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (updated on 18/03/2018)
  * targets:
    * setup_giants_2.1.0.4.exe (MD5: 33015108ece9e52b1f525880f0867e11)
  * dependencies:
    * fakeroot
    * innoextract
    * icoutils (optional)

<note>The game installed via these scripts will use [[https://www.winehq.org/|WINE]].</note>

==== Usage ====

  - Install the scripts dependencies:<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
play-anything.sh
play-giants-citizen-kabuto_gog-2.1.0.4.sh
setup_giants_2.1.0.4.exe
</code>
  - Start the building process from this directory:<code>
$ sh ./play-giants-citizen-kabuto_gog-2.1.0.4.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Advanced usage ====

This script accept several options to set its behaviour. Follow the links to more details on how to use them.

  * [[:en:common:advanced-options#checksum|checksum]]
  * [[:en:common:advanced-options#compression|compression]]
  * [[:en:common:advanced-options#help|help]]
  * [[:en:common:advanced-options#prefix|prefix]]

==== Configure the WINE prefix ====

You can access the WINE configuration window for the respective games prefixes via the following command:
<code>
giants-winecfg
</code>
Any change made here will be specific to the game, it won’t have any effect on your other WINE games even if you didn’t install them via a ./play.it script.

==== Play in a window ====

The following method will give you a way to run the game in a window:
  - Open the WINE prefix configuration window;
  - In the configuration window, go on the "Graphics" tab;
  - Check "Emulate a virtual desktop", and put your desktop resolution in the "Desktop size" fields;
  - Close the window with "OK".
Done, from the next launch Giants: Citizen Kabuto will run in a window that will automatically scale to its resolution. You can go back to fullscreen mode at any time by running the WINE configuration screen and uncheck "Emulate a virtual desktop".

==== Links ====

  * [[https://en.wikipedia.org/wiki/Giants:_Citizen_Kabuto|Wikipedia article]]
  * [[https://appdb.winehq.org/objectManager.php?sClass=version&iId=29132|WineHQ AppDB entry]]
