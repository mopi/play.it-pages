====== Darkest Dungeon ======

===== Description =====

[[https://www.dotslashplay.it/images/games/darkest-dungeon/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/darkest-dungeon/thumbnail.jpg?nocache }}]]

  * age: 16y+
  * category: RPG, Fantasy, turn-based
  * official url: [[https://www.darkestdungeon.com/|]]
  * release date: 2016

===== Informations =====

=== Darkest Dungeon ===

**Darkest Dungeon**: Recruit, train and lead a team of heroes with their qualities and defects (especially their flaws) through cursed forests, forgotten labyrinths, decayed crypts and much worse.\\
You will face not only formidable adversaries but also stress, famine, disease and darkness.\\
Discover strange secrets and defeat terrifying monsters using the strategic combat system.

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-darkest-dungeon.sh|play-darkest-dungeon.sh]] (updated on 4/11/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 18/03/2018)
  * target:
    * [[https://www.gog.com/game/darkest_dungeon|darkest_dungeon_en_21142_16140.sh]] (MD5: 4b43065624dbab74d794c56809170588)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

=== The Crimson Court ===

**Dararkest Dungeon: The Crimson Court** is the first extension of the Gothic role-playing game.\\
Crimson Court offers a unique campaign, along with Darkest Dungeon’s main content, including new challenges and different levels of difficulty.\\
The story, which explores the ancestor’s past, is presented in the form of cut scenes like the rest of the game.

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-darkest-dungeon-the-crimson-court.sh|play-darkest-dungeon-the-crimson-court.sh]] (updated on 1/11/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 18/03/2018)
  * target:
    * [[https://www.gog.com/game/darkest_dungeon_the_crimson_court|darkest_dungeon_the_crimson_court_dlc_en_21096_16065.sh]] (MD5: d4beaeb7effff0cbd2e292abf0ef5332)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

==== Usage (Darkest Dungeon) ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
darkest_dungeon_en_21142_16140.sh
libplayit2.sh
play-darkest-dungeon.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-darkest-dungeon.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Usage (The Crimson Court) ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
darkest_dungeon_the_crimson_court_dlc_en_21096_16065.sh
libplayit2.sh
play-darkest-dungeon-the-crimson-court.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-darkest-dungeon-the-crimson-court.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
