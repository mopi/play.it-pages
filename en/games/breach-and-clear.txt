====== Breach & Clear ======

===== Description =====

[[https://www.dotslashplay.it/images/games/breach-and-clear/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/breach-and-clear/thumbnail.jpg?nocache }}]]

**Breach & Clear**: Build your Special Operations team, plan and execute advanced missions, and own every angle.

  * age: 17y+
  * category: strategy, RPG, action, simulation
  * official url: [[https://www.breachandclear.com/|]]
  * release date: 2013

===== Informations =====

//version sold on GOG//

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-breach-and-clear_gog-2.0.0.3.sh|play-breach-and-clear_gog-2.0.0.3.sh]] (updated on 5/06/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (updated on 18/03/2018)
  * target:
    * gog_breach_clear_2.0.0.3.sh (MD5: e7f6b59d8ce97c9ab850b47608941b7d)
  * dependencies:
    * fakeroot
    * unzip

==== Usage ====

  - Install the scripts dependencies:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory the scripts and installer:<code>
$ ls
</code><code>
gog_breach_clear_2.0.0.3.sh
play-anything.sh
play-breach-and-clear_gog-2.0.0.3.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-breach-and-clear_gog-2.0.0.3.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Advanced usage ====

This script accept several options to set its behaviour. Follow the links to more details on how to use them.

  * [[:en:common:advanced-options#checksum|checksum]]
  * [[:en:common:advanced-options#compression|compression]]
  * [[:en:common:advanced-options#help|help]]
  * [[:en:common:advanced-options#prefix|prefix]]

==== Links ====

  * [[https://en.wikipedia.org/wiki/Breach_%26_Clear_(video_game)|Wikipedia article]]
