====== Grim Fandango ======

===== Description =====

[[https://www.dotslashplay.it/images/games/grim-fandango/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/grim-fandango/thumbnail.jpg?nocache }}]]

**Grim Fandango**: Something’s rotten in the land of the dead, and you're being played for a sucker.\\
Meet Manny Calavera, travel agent at the Department of Death. He sells luxury packages to souls on their four-year journey to eternal rest. But there’s trouble in paradise.\\
Help Manny untangle himself from a conspiracy that threatens his very salvation.

  * age: 13y+
  * category: Adventure, Point’n Click, Mystery
  * official url: [[http://grimremastered.com/|]]
  * release date: 2015

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-grim-fandango-remastered.sh|play-grim-fandango-remastered.sh]] (updated on 24/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 18/03/2018)
  * target:
    * [[https://www.gog.com/game/grim_fandango_remastered|gog_grim_fandango_remastered_2.3.0.7.sh]] (MD5: 9c5d124c89521d254b0dc259635b2abe)
  * dependencies:
    * Arch Linux:
      * unzip
    * Debian:
      * fakeroot
      * unzip

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S unzip
</code>
    - Debian:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
gog_grim_fandango_remastered_2.3.0.7.sh
libplayit2.sh
play-grim-fandango-remastered.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-grim-fandango-remastered.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
