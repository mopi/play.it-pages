====== Shelter ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-shelter.sh|play-shelter.sh]] (updated on 6/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 18/03/2018)
  * target:
    * [[https://www.gog.com/game/shelter|setup_shelter_2.0.0.6.exe]] (MD5: 06860af1df9a8120bce4e97d899b3edd)
  * dependencies:
    * Arch Linux:
      * icoutils
      * innoextract
    * Debian:
      * fakeroot
      * icoutils
      * innoextract

[[https://www.dotslashplay.it/images/games/shelter/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/shelter/thumbnail.jpg?nocache }}]]

<note>The game installed via these scripts will use [[https://www.winehq.org/|WINE]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
$ yaourt icoutils innoextract
</code>
    - Debian:<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-shelter.sh
setup_shelter_2.0.0.6.exe
</code>
  - Start the building process from this directory:<code>
$ sh ./play-shelter.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
