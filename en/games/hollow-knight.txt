====== Hollow Knight ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-hollow-knight.sh|play-hollow-knight.sh]] (updated on 24/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 18/03/2018)
  * target:
    * [[https://www.gog.com/game/hollow_knight|gog_hollow_knight_2.1.0.2.sh]] (MD5: 0d18baf29d5552dc094ca2bfe5fcaae6)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/hollow-knight/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/hollow-knight/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
gog_hollow_knight_2.1.0.2.sh
libplayit2.sh
play-hollow-knight.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-hollow-knight.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
