====== Metal Slug 3 ======
//version sold on Humble Bundle//

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-metal-slug-3_humble-2014-06-09.sh|play-metal-slug-3_humble-2014-06-09.sh]] (updated on 22/04/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (updated on 18/03/2018)
  * target:
    * MetalSlug3-Linux-2014-06-09.sh (MD5: a8a3aee4e3438d2d6d5bab23236e43a3)
  * dependencies:
    * fakeroot
    * unzip

[[https://www.dotslashplay.it/images/games/metal-slug-3/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/metal-slug-3/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory the scripts and installer:<code>
$ ls
</code><code>
MetalSlug3-Linux-2014-06-09.sh
play-anything.sh
play-metal-slug-3_humble-2014-06-09.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-metal-slug-3_humble-2014-06-09.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Advanced usage ====

This script accept several options to set its behaviour. Follow the links to more details on how to use them.

  * [[:en:common:advanced-options#checksum|checksum]]
  * [[:en:common:advanced-options#compression|compression]]
  * [[:en:common:advanced-options#help|help]]
  * [[:en:common:advanced-options#prefix|prefix]]

==== Links ====

  * [[https://en.wikipedia.org/wiki/Metal_Slug_3|Wikipedia article]]
