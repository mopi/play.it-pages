====== Space Pirates And Zombies ======
//version sold on GOG//

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (updated on 18/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-space-pirates-and-zombies.sh|play-space-pirates-and-zombies.sh]] (updated on 16/05/2017)
  * target:
    * gog_space_pirates_and_zombies_2.0.0.4.sh (MD5: 46da2a84e78f8016e35f7c0e63e28581)
  * dependencies:
    * fakeroot
    * unzip

[[https://www.dotslashplay.it/images/games/space-pirates-and-zombies/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/space-pirates-and-zombies/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory scripts and installer:<code>
$ ls
</code><code>
play-anything.sh
play-space-pirates-and-zombies.sh
gog_space_pirates_and_zombies_2.0.0.4.sh
</code>
  - Run the building process:<code>
$ sh ./play-space-pirates-and-zombies.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game.

==== Advanced usage ====

This script accept several options to set its behaviour. Follow the links to more details on how to use them.

  * [[:en:common:advanced-options#checksum|checksum]]
  * [[:en:common:advanced-options#compression|compression]]
  * [[:en:common:advanced-options#help|help]]
  * [[:en:common:advanced-options#prefix|prefix]]

==== Links ====

[[https://en.wikipedia.org/wiki/Space_Pirates_and_Zombies|SPAZ on Wikipedia]]
