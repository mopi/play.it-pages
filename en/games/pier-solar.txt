====== Pier Solar and the Great Architects ======
//version sold on GOG//

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-pier-solar_gog-2.1.0.4.sh|play-pier-solar_gog-2.1.0.4.sh]] (updated on 12/06/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (updated on 18/03/2018)
  * target:
    * gog_pier_solar_and_the_great_architects_2.1.0.4.sh (MD5: 2de03fb6d69944e3f204d5ae45147a3e)
  * dependencies:
    * fakeroot
    * unzip

[[https://www.dotslashplay.it/images/games/pier-solar-hd/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/pier-solar-hd/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory the scripts and installer:<code>
$ ls
</code><code>
gog_pier_solar_and_the_great_architects_2.1.0.4.sh
play-anything.sh
play-pier-solar_gog-2.1.0.4.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-pier-solar_gog-2.1.0.4.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Advanced usage ====

This script accept several options to set its behaviour. Follow the links to more details on how to use them.

  * [[:en:common:advanced-options#checksum|checksum]]
  * [[:en:common:advanced-options#compression|compression]]
  * [[:en:common:advanced-options#help|help]]
  * [[:en:common:advanced-options#prefix|prefix]]

==== Links ====

  * [[https://en.wikipedia.org/wiki/Pier_Solar_and_the_Great_Architects|Wikipedia article]]
