====== Atlantis: The Lost Tales ======

===== Description =====

[[https://www.dotslashplay.it/images/games/atlantis-1-the-lost-tales/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/atlantis-1-the-lost-tales/thumbnail.jpg?nocache }}]]

**Atlantis: The Lost Tales** is a fantasy adventure.\\
The game begins with Seth, the protagonist, joining the Queen’s Companions, the personal guardians of the Queen of Atlantis.\\
He discovers after he arrives that the Queen disappeared shortly before he joined…

  * category: puzzle, adenture, first-person perspective
  * release date: 1997

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-atlantis-1-the-lost-tales.sh|play-atlantis-1-the-lost-tales.sh]] (updated on 30/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 18/03/2018)
  * targets:
    * English version:
      * [[https://www.gog.com/game/atlantis_the_lost_tales|setup_atlantis_the_lost_tales_2.0.0.15.exe]] (MD5: 287170bea9041b4e29888d97f87eb9fc)
    * French version:
      * [[https://www.gog.com/game/atlantis_the_lost_tales|setup_atlantis_the_lost_tales_french_2.1.0.15.exe]] (MD5: 0cb6b037a457d35dacd23e1f22aea57b)
  * dependencies:
    * Arch Linux:
      * icoutils
      * innoextract
    * Debian:
      * fakeroot
      * icoutils
      * innoextract

<note>The game installed via these scripts will use [[https://www.winehq.org/|WINE]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
$ yaourt icoutils innoextract
</code>
    - Debian:<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Put in a same directory the scripts and archive:
    - English version:<code>
$ ls
</code><code>
libplayit2.sh
play-atlantis-1-the-lost-tales.sh
setup_atlantis_the_lost_tales_2.0.0.15.exe
</code>
    - French version:<code>
$ ls
</code><code>
libplayit2.sh
play-atlantis-1-the-lost-tales.sh
setup_atlantis_the_lost_tales_french_2.1.0.15.exe
</code>
  - Start the building process from this directory:<code>
$ sh ./play-atlantis-1-the-lost-tales.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
