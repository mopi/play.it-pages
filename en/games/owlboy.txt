====== Owlboy ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-owlboy.sh|play-owlboy.sh]] (updated on 9/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 18/03/2018)
  * target:
    * [[https://www.humblebundle.com/store/owlboy|owlboy-05232017-bin]] (MD5: d3a1e4753a604431c58eb1ea26c35543)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/owlboy/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/owlboy/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
owlboy-05232017-bin
play-owlboy.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-owlboy.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
