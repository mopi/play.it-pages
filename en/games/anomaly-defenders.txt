====== Anomaly Defenders ======

===== Description =====

[[https://www.dotslashplay.it/images/games/anomaly-defenders/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/anomaly-defenders/thumbnail.jpg?nocache }}]]

**Anomaly Defenders** is the closing installment of the Anomaly series.\\
The original incarnation of the Tower Offense sub-genre had players controlling humans fighting alien invaders.\\
Now, the tables have turned. The human counterattack is underway and the alien homeworld is under threat. Defend the planet from the human scum in the final battle of the series.

  * category: strategy, action, tower defense,
  * official url: [[http://www.anomalythegame.com/#AD|]]
  * release date: 2014

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-anomaly-defenders.sh|play-anomaly-defenders.sh]] (updated on 8/09/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 18/03/2018)
  * targets:
    * [[https://www.humblebundle.com/store/anomaly-defenders|AnomalyDefenders_Linux_1402512837.tar.gz]] (MD5: 35ccd57e8650dd53a09b1f1e088307cc)
  * dependencies:
    * Debian:
      * fakeroot

==== Usage ====

  - Install the scripts dependencies:
    - Debian:<code>
# apt-get install fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
AnomalyDefenders_Linux_1402512837.tar.gz
libplayit2.sh
play-anomaly-defenders.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-anomaly-defenders.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
