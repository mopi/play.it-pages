====== Aladdin ======

===== Description =====

[[https://www.dotslashplay.it/images/games/aladdin/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/aladdin/thumbnail.jpg?nocache }}]]

**Aladdin** must make his way through several levels based on locations from the movie: from the streets and rooftops of Agrabah, the Cave of Wonders and the Sultan’s dungeon to the final confrontation in Grand Vizier Jafar’s palace.

  * category: platform, action
  * release date: 1993

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-aladdin.sh|play-aladdin.sh]] (updated on 28/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 18/03/2018)
  * target:
    * [[https://www.gog.com/game/disney_aladdin|gog_disney_s_aladdin_2.0.0.2.sh]] (MD5: 9dd6d84c2276809c5630320335e3415b)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

<note>The game installed via these scripts will run through [[https://www.dosbox.com/|DOSBox]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
gog_disney_s_aladdin_2.0.0.2.sh
libplayit2.sh
play-aladdin.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-aladdin.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Links ====

  * [[https://en.wikipedia.org/wiki/Disney%27s_Aladdin_(Virgin_Games)|Wikipedia article]]
