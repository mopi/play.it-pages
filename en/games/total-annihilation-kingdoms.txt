====== Total Annihilation Kingdoms ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-total-annihilation-kingdoms.sh|play-total-annihilation-kingdoms.sh]] (updated on 7/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 18/03/2018)
  * target:
    * [[https://www.gog.com/game/total_annihilation_kingdoms|setup_total_annihilation_kingdoms_2.0.0.22.exe]] (MD5: e0eb1f17ca2285fc3de10e16b394bfb0)
  * dependencies:
    * Arch Linux:
      * icoutils
      * innoextract
    * Debian:
      * fakeroot
      * icoutils
      * innoextract

<note>The game installed via these scripts will use [[https://www.winehq.org/|WINE]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
$ yaourt icoutils innoextract
</code>
    - Debian:<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-total-annihilation-kingdoms.sh
setup_total_annihilation_kingdoms_2.0.0.22.exe
</code>
  - Start the building process from this directory:<code>
$ sh ./play-total-annihilation-kingdoms.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
