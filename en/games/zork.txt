====== Zork: The Great Underground Empire ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-zork.sh|play-zork.sh]] (updated on 23/05/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 18/03/2018)
  * target:
    * [[https://www.gog.com/game/the_zork_anthology|setup_zork_2.1.0.17.exe]] (MD5: 6bd42788d696f154ce363830909259d3)
  * dependencies:
    * Arch Linux:
      * icoutils
      * innoextract
    * Debian:
      * fakeroot
      * icoutils
      * innoextract

[[https://www.dotslashplay.it/images/games/zork/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/zork/thumbnail.jpg?nocache }}]]

<note>The game installed via these scripts will run through [[https://www.dosbox.com/|DOSBox]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
$ yaourt -S icoutils innoextract
</code>
    - Debian:<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-zork.sh
setup_zork_2.1.0.17.exe
</code>
  - Start the building process from this directory:<code>
$ sh ./play-zork.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Links ====

  * [[https://en.wikipedia.org/wiki/Zork|Wikipedia article]]
