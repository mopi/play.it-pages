====== TRI: Of Friendship and Madness ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-tri-of-friendship-and-madness.sh|play-tri-of-friendship-and-madness.sh]] (updated on 3/07/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 18/03/2018)
  * target:
    * [[https://www.gog.com/game/tri|gog_tri_2.6.0.9.sh]] (MD5: c2bf151b58766740e52db9559d61e3d6)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/tri-of-friendship-and-madness/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/tri-of-friendship-and-madness/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
gog_tri_2.6.0.9.sh
libplayit2.sh
play-tri-of-friendship-and-madness.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-tri-of-friendship-and-madness.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
