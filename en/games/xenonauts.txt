====== Xenonauts ======

=== version sold on GOG ===

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-xenonauts_gog-2.1.0.3.sh|play-xenonauts_gog-2.1.0.3.sh]] (updated on 15/04/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (updated on 18/03/2018)
  * target:
    * gog_xenonauts_2.1.0.3.sh (MD5: 75a6b10e624988e8e908630995ddfdb5)
  * dependencies:
    * fakeroot
    * unzip

=== version sold on Humble Bundle ===

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-xenonauts_humble-2016-03-03.sh|play-xenonauts_humble-2016-03-03.sh]] (updated on 15/04/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (updated on 18/03/2018)
  * target:
    * Xenonauts-DRMFree-Linux-2016-03-03.sh (MD5: f4369e987381b84fde64be569fbab913)
  * dependencies:
    * fakeroot
    * unzip

[[https://www.dotslashplay.it/images/games/xenonauts/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/xenonauts/thumbnail.jpg?nocache }}]]

==== Usage (GOG version) ====

  - Install the scripts dependencies:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory scripts and installer:<code>
$ ls
</code><code>
gog_xenonauts_2.1.0.3.sh
play-anything.sh
play-xenonauts_gog-2.1.0.3.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-xenonauts_gog-2.1.0.3.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Usage (Humble version) ====

  - Install the scripts dependencies:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory scripts and installer:<code>
$ ls
</code><code>
play-anything.sh
play-xenonauts_humble-2016-03-03.sh
Xenonauts-DRMFree-Linux-2016-03-03.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-xenonauts_humble-2016-03-03.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Advanced usage ====

This script accept several options to set its behaviour. Follow the links to more detai$ ls on how to use them.

  * [[:en:common:advanced-options#checksum|checksum]]
  * [[:en:common:advanced-options#compression|compression]]
  * [[:en:common:advanced-options#help|help]]
  * [[:en:common:advanced-options#prefix|prefix]]

==== Links ====

  * [[https://en.wikipedia.org/wiki/Xenonauts|Wikipedia article]]
