====== Shadowrun Returns ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-shadowrun-returns.sh|play-shadowrun-returns.sh]] (updated on 17/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 18/03/2018)
  * targets:
    * GOG:
      * [[https://www.gog.com/game/shadowrun_returns|gog_shadowrun_returns_2.0.0.7.sh]] (MD5: 61c12b14c7e6040cb1465390320a61da)
    * Humble Bundle:
      * [[https://www.humblebundle.com/store/shadowrun-returns|shadowrun-returns-linux127.tar.gz]] (MD5: ff3146b1ad046f81bf8f3deba277e472)
  * dependencies:
    * GOG:
      * Arch Linux:
        * libarchive
      * Debian:
        * bsdtar
        * fakeroot
    * Humble Bundle:
      * Debian:
        * fakeroot

[[https://www.dotslashplay.it/images/games/shadowrun-returns/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/shadowrun-returns/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - GOG:
      - Arch Linux:<code>
# pacman -S libarchive
</code>
      - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
    - Humble Bundle:
      - Debian:<code>
# apt-get install fakeroot
</code>
  - Put in a same directory the scripts and archive:
    - GOG:<code>
$ ls
</code><code>
gog_shadowrun_returns_2.0.0.7.sh
libplayit2.sh
play-shadowrun-returns.sh
</code>
    - Humble Bundle:<code>
$ ls
</code><code>
libplayit2.sh
play-shadowrun-returns.sh
shadowrun-returns-linux127.tar.gz
</code>
  - Start the building process from this directory:<code>
$ sh ./play-shadowrun-returns.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
