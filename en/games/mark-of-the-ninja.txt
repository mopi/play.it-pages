====== Mark of the Ninja ======

=== Mark of the Ninja ===

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-mark-of-the-ninja.sh|play-mark-of-the-ninja.sh]] (updated on 3/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 18/03/2018)
  * targets:
    * GOG:
      * [[https://www.gog.com/game/mark_of_the_ninja|gog_mark_of_the_ninja_2.0.0.4.sh]] (MD5: 126ded567b38580f574478fd994e3728)
    * Humble Bundle:
      * markoftheninja_linux38_1380755375.zip (MD5: 7871a48068ef43e93916325eedd6913e)
  * dependencies:
    * Arch Linux:
      * GOG:
        * libarchive
      * Humble Bundle:
        * unzip
    * Debian:
      * GOG:
        * bsdtar
        * fakeroot
      * Humble Bundle:
        * fakeroot
        * unzip

=== Special Edition DLC ===

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-mark-of-the-ninja-special-edition.sh|play-mark-of-the-ninja-special-edition.sh]] (updated on 3/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 18/03/2018)
  * target:
    * [[https://www.gog.com/game/mark_of_the_ninja_special_edition_upgrade|gog_mark_of_the_ninja_special_edition_dlc_2.0.0.4.sh]] (MD5: bbce70b80932ec9c14fbedf0b6b33eb1)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/mark-of-the-ninja/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/mark-of-the-ninja/thumbnail.jpg?nocache }}]]

==== Usage (Mark of the Ninja) ====

  - Install the scripts dependencies:
    - Arch Linux:
      - GOG:<code>
# pacman -S libarchive
</code>
      - Humble Bundle:<code>
# pacman -S unzip
</code>
    - Debian:
      - GOG:<code>
# apt-get install bsdtar fakeroot
</code>
      - Humble Bundle:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory the scripts and archive:
    - GOG:<code>
$ ls
</code><code>
gog_mark_of_the_ninja_2.0.0.4.sh
libplayit2.sh
play-mark-of-the-ninja.sh
</code>
    - Humble Bundle:<code>
$ ls
</code><code>
libplayit2.sh
play-mark-of-the-ninja.sh
markoftheninja_linux38_1380755375.zip
</code>
  - Start the building process from this directory:<code>
$ sh ./play-mark-of-the-ninja.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Usage (Special Edition DLC) ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
gog_mark_of_the_ninja_special_edition_dlc_2.0.0.4.sh
libplayit2.sh
play-mark-of-the-ninja-special-edition.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-mark-of-the-ninja-special-edition.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
