====== unEpic ======

=== version sold on GOG ===

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-unepic_gog-2.1.0.4.sh|play-unepic_gog-2.1.0.4.sh]] (updated on 5/06/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (updated on 18/03/2018)
  * target:
    * gog_unepic_2.1.0.4.sh (MD5: 341556e144d5d17ae23d2b0805c646a1)
  * dependencies:
    * fakeroot
    * unzip

=== version sold on Humble Bundle ===

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-unepic_humblebundle-2014-12-08.sh|play-unepic_humblebundle-2014-12-08.sh]]
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (updated on 18/03/2018)
  * target:
    * unepic-15005.run (MD5: 940824c4de6e48522845f63423e87783)
  * dependencies:
    * fakeroot
    * unzip

[[https://www.dotslashplay.it/images/games/unepic/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/unepic/thumbnail.jpg?nocache }}]]

==== Usage (GOG version) ====

  - Install the scripts dependencies:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory the scripts and installer:<code>
$ ls
</code><code>
gog_unepic_2.1.0.4.sh
play-anything.sh
play-unepic_gog-2.1.0.4.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-unepic_gog-2.1.0.4.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Usage (Humble version) ====

  - Install the scripts dependencies:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory the scripts and installer:<code>
$ ls
</code><code>
play-anything.sh
play-unepic_humblebundle-2014-12-08.sh
unepic-15005.run
</code>
  - Start the building process from this directory:<code>
$ sh ./play-unepic_humblebundle-2014-12-08.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Advanced usage ====

This script accept several options to set its behaviour. Follow the links to more details on how to use them.

  * [[:en:common:advanced-options#checksum|checksum]]
  * [[:en:common:advanced-options#compression|compression]]
  * [[:en:common:advanced-options#help|help]]
  * [[:en:common:advanced-options#prefix|prefix]]

==== Links ====

  * [[https://en.wikipedia.org/wiki/Unepic|Wikipedia article]]
