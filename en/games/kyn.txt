====== Kyn ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-kyn.sh|play-kyn.sh]] (updated on 18/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 18/03/2018)
  * targets:
    * [[https://www.gog.com/game/kyn|setup_kyn_update_4_(17655).exe]] (MD5: ca2a665c27ef02f0bfa4e72dc368952c)
    * [[https://www.gog.com/game/kyn|setup_kyn_update_4_(17655)-1.bin]] (MD5: 8c6c13b15a4d39ac9389c7c4f9ce6760)
  * dependencies:
    * Arch Linux:
      * icoutils
      * innoextract
    * Debian:
      * fakeroot
      * icoutils
      * innoextract

[[https://www.dotslashplay.it/images/games/kyn/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/kyn/thumbnail.jpg?nocache }}]]

<note>The game installed via these scripts will use [[https://www.winehq.org/|WINE]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
$ yaourt icoutils innoextract
</code>
    - Debian:<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-kyn.sh
setup_kyn_update_4_(17655).exe
setup_kyn_update_4_(17655)-1.bin
</code>
  - Start the building process from this directory:<code>
$ sh ./play-kyn.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
