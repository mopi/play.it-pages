====== Le Maître de l’Olympe : Zeus + Le Maître de l’Atlantide : Poséidon ======

<note important>Les archives de traduction proposées ici sont destinées à être appliquées sur une installation de Zeus + Poséidon (alias "Zeus Gold" ou "Acropolis"). Elles n’ont pas été testées sur une installation de Zeus sans son extension.</note>

[[https://www.dotslashplay.it/traductions/zeus/|index des archives de traduction]]

Vous trouverez sur la page donnée en lien trois archives de traduction :
  * zeusfr-textes.7z (~3 Mio) traduit les textes
  * zeusfr-videos.7z (~63 Mio) traduit les vidéos
  * zeusfr-voix.7z (~180 Mio) traduit les voix
Pour une traduction complète du jeu vous devrez donc télécharger les trois archives.

Une fois les archives qui vous intéressent téléchargées, il vous suffit de les décompresser à la racine du répertoire d’installation du jeu en remplaçant les fichiers déjà en place pour passer votre jeu en français.

=== bug connu ===

L’archive de traduction des textes n’apporte pas une gestion correcte des caractères accentués, ce qui donnera un effet similaire à celui sur la capture suivante :

[[https://www.dotslashplay.it/images/games/zeus-master-of-olympus/tradfr-menu.jpg|{{https://www.dotslashplay.it/images/games/zeus-master-of-olympus/tradfr-menu_small.jpg?nocache}}]]
