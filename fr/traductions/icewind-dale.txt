====== Icewind Dale ======

[[https://www.dotslashplay.it/traductions/icewind-dale/|index des archives de traduction]]

Vous trouverez sur la page donnée en lien trois archives de traduction :
  * iwd1fr-txt.7z (~23 Mio) traduit les textes
  * iwd1fr-snd.7z (~110 Mio) traduit les voix
  * iwd1fr-vid.7z (~250 Mio) traduit les vidéos

Une fois les archives qui vous intéressent téléchargées, il vous suffit de les décompresser à la racine du répertoire d’installation du jeu en remplaçant les fichiers déjà en place pour passer votre jeu en français.
