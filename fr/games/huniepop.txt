====== HuniePop ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-huniepop.sh|play-huniepop.sh]] (mis à jour le 10/02/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cible :
    * [[https://www.gog.com/game/huniepop|gog_huniepop_2.0.0.3.sh]] (MD5 : d229aea2b601137537f7be46c7327660)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/huniepop/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/huniepop/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_huniepop_2.0.0.3.sh
libplayit2.sh
play-huniepop.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-huniepop.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
