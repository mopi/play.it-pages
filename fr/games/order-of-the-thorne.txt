====== Order of the Thorne: The King’s Challenge ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-order-of-the-thorne.sh|play-order-of-the-thorne.sh]] (mis à jour le 3/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cible :
    * [[https://www.gog.com/game/order_of_the_thorne_the_kings_challenge|gog_order_of_the_thorne_the_king_s_challenge_2.0.0.1.sh]] (MD5 : 16dde031dcfb730be1d94bde77306b0d)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/order-of-the-thorne/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/order-of-the-thorne/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_order_of_the_thorne_the_king_s_challenge_2.0.0.1.sh
libplayit2.sh
play-order-of-the-thorne.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-order-of-the-thorne.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
