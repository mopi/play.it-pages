====== Retro City Rampage ======
//version vendue sur Humble Bundle//

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-retro-city-rampage.sh|play-retro-city-rampage.sh]] (mis à jour le 28/08/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (mis à jour le 18/03/2018)
  * cible :
    * version 32-bit :
      * retrocityrampage-1.0-linux.i386.bin (MD5 : 35c776fa33af850158b0d6a886dfe2a0)
    * version 64-bit :
      * retrocityrampage-1.0-linux.x86_64.bin (MD5 : 4fc25ab742d5bd389bd4a76eb6ec987f)
  * dépendance :
    * fakeroot

[[https://www.dotslashplay.it/images/games/retro-city-rampage/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/retro-city-rampage/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :<code>
# apt-get install fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’installeur :
    - version 32-bit :<code>
$ ls
</code><code>
play-anything.sh
play-retro-city-rampage.sh
retrocityrampage-1.0-linux.i386.bin
</code>
    - version 64-bit :<code>
$ ls
</code><code>
play-anything.sh
play-retro-city-rampage.sh
retrocityrampage-1.0-linux.x86_64.bin
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-retro-city-rampage.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Utilisation avancée ====

Le script présenté ici peut prendre plusieurs options pour contrôler son comportement de manière plus fine. Suivez les liens pour avoir des détails sur leur utilisation.

  * [[:fr:commun:options-avancees#checksum|checksum]]
  * [[:fr:commun:options-avancees#compression|compression]]
  * [[:fr:commun:options-avancees#help|help]]
  * [[:fr:commun:options-avancees#prefix|prefix]]

==== Liens ====

  * [[https://en.wikipedia.org/wiki/Retro_City_Rampage|article Wikipedia]] (en anglais)
  * [[http://www.retrocityrampage.com/|site officiel]] (en anglais)
