====== The Lion King ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-the-lion-king.sh|play-the-lion-king.sh]] (mis à jour le 28/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cible :
    * [[https://www.gog.com/game/disney_the_lion_king|gog_the_lion_king_2.0.0.2.sh]] (MD5 : 3b4f1118785e1f1cc769ae41379b7940)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/the-lion-king/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/the-lion-king/thumbnail.jpg?nocache }}]]

<note>Le jeu installé via ce script utilisera [[https://www.dosbox.com/|DOSBox]].</note>

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_the_lion_king_2.0.0.2.sh
libplayit2.sh
play-the-lion-king.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-the-lion-king.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f

==== Liens ====

  * [[https://fr.wikipedia.org/wiki/Le_Roi_lion_(jeu_vidéo,_1994)|article Wikipédia]]
