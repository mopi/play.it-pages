====== The Night of the Rabbit ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-the-night-of-the-rabbit.sh|play-the-night-of-the-rabbit.sh]] (mis à jour le 19/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cibles :
    * [[https://www.gog.com/game/the_night_of_the_rabbit|setup_the_night_of_the_rabbit_2.1.0.5-1.bin]] (MD5 : 565c8c59266eced8483ad579ecf3c454)
    * [[https://www.gog.com/game/the_night_of_the_rabbit|setup_the_night_of_the_rabbit_2.1.0.5-2.bin]] (MD5 : 403e06a8e8aef71989bf550369244373)
  * dépendances :
    * Arch Linux :
      * icoutils
      * unarchiver
    * Debian :
      * fakeroot
      * icoutils
      * unar

[[https://www.dotslashplay.it/images/games/the-night-of-the-rabbit/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/the-night-of-the-rabbit/thumbnail.jpg?nocache }}]]

<note>Le jeu installé via ce script utilisera [[https://www.winehq.org/|WINE]].</note>

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
$ yaourt icoutils unarchiver
</code>
    - Debian :<code>
# apt-get install fakeroot icoutils unar
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
libplayit2.sh
play-the-night-of-the-rabbit.sh
setup_the_night_of_the_rabbit_2.1.0.5-1.bin
setup_the_night_of_the_rabbit_2.1.0.5-2.bin
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-the-night-of-the-rabbit.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
