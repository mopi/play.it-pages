====== About Love, Hate and the Other Ones ======

===== Description =====

[[https://www.dotslashplay.it/images/games/about-love-hate-and-the-other-ones/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/about-love-hate-and-the-other-ones/thumbnail.jpg?nocache }}]]

**About Love, Hate and the other one** est un projet qui joue avec les deux plus intenses émotions : AMOUR et HAINE.\\
Vous contrôlez deux personnages dans une succession de niveau pour atteindre un bouton rouge.

  * age: +3 ans
  * category: réflexion
  * official url: [[http://www.aboutloveandhate.de/|]]
  * date de sortie : 2012

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-about-love-hate-and-the-other-ones.sh|play-about-love-hate-and-the-other-ones.sh]] (mis à jour le 22/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cible :
    * [[https://www.humblebundle.com/store/about-love-hate-and-the-other-ones|aboutloveandhate-1.3.1.deb]] (MD5 : 65c314a2a970b5c787d4e7e2a837e211)
  * dépendances :
    * Arch Linux :
      * dpkg
    * Debian :
      * fakeroot

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S dpkg
</code>
    - Debian :<code>
# apt-get install fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
aboutloveandhate-1.3.1.deb
libplayit2.sh
play-about-love-hate-and-the-other-ones.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-about-love-hate-and-the-other-ones.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
