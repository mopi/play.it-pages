====== Heroes Chronicles ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-heroes-chronicles.sh|play-heroes-chronicles.sh]] (mis à jour le 5/02/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cibles :
    * Chapitre 1 :
      * [[https://www.gog.com/game/heroes_chronicles_all_chapters|setup_heroes_chronicles_chapter1_2.1.0.42.exe]] (MD5 : f584d6e11ed47d1d40e973a691adca5d)
    * Chapitre 2 :
      * [[https://www.gog.com/game/heroes_chronicles_all_chapters|setup_heroes_chronicles_chapter2_2.1.0.43.exe]] (MD5 : 0d240bc0309814ba251c2d9b557cf69f)
    * Chapitre 3 :
      * [[https://www.gog.com/game/heroes_chronicles_all_chapters|setup_heroes_chronicles_chapter3_2.1.0.41.exe]] (MD5 : cb21751572960d47a259efc17b92c88c)
    * Chapitre 4 :
      * [[https://www.gog.com/game/heroes_chronicles_all_chapters|setup_heroes_chronicles_chapter4_2.1.0.42.exe]] (MD5 : 922291e16176cb4bd37ca88eb5f3a19e)
    * Chapitre 5 :
      * [[https://www.gog.com/game/heroes_chronicles_all_chapters|setup_heroes_chronicles_chapter5_2.1.0.42.exe]] (MD5 : 57b3ec588e627a2da30d3bc80ede5b1d)
    * Chapitre 6 :
      * [[https://www.gog.com/game/heroes_chronicles_all_chapters|setup_heroes_chronicles_chapter6_2.1.0.42.exe]] (MD5 : 64becfde1882eecd93fb02bf215eff11)
    * Chapitre 7 :
      * [[https://www.gog.com/game/heroes_chronicles_all_chapters|setup_heroes_chronicles_chapter7_2.1.0.42.exe]] (MD5 : 07c189a731886b2d3891ac1c65581d40)
    * Chapitre 8 :
      * [[https://www.gog.com/game/heroes_chronicles_all_chapters|setup_heroes_chronicles_chapter8_2.1.0.42.exe]] (MD5 : 2b3e4c366db0f7e3e8b15b0935aad528)
  * dépendances :
    * Arch Linux :
      * icoutils
      * innoextract
    * Debian :
      * fakeroot
      * icoutils
      * innoextract

[[https://www.dotslashplay.it/images/games/heroes-chronicles/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/heroes-chronicles/thumbnail.jpg?nocache }}]]

<note>Le jeu installé via ce script utilisera [[https://www.winehq.org/|WINE]].</note>

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
$ yaourt icoutils innoextract
</code>
    - Debian :<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Placez dans un même répertoire les scripts et l’archive :
    - Chapitre 1 :<code>
$ ls
</code><code>
libplayit2.sh
play-heroes-chronicles.sh
setup_heroes_chronicles_chapter1_2.1.0.42.exe
</code>
    - Chapitre 2 :<code>
$ ls
</code><code>
libplayit2.sh
play-heroes-chronicles.sh
setup_heroes_chronicles_chapter2_2.1.0.43.exe
</code>
    - Chapitre 3 :<code>
$ ls
</code><code>
libplayit2.sh
play-heroes-chronicles.sh
setup_heroes_chronicles_chapter3_2.1.0.41.exe
</code>
    - Chapitre 4 :<code>
$ ls
</code><code>
libplayit2.sh
play-heroes-chronicles.sh
setup_heroes_chronicles_chapter4_2.1.0.42.exe
</code>
    - Chapitre 5 :<code>
$ ls
</code><code>
libplayit2.sh
play-heroes-chronicles.sh
setup_heroes_chronicles_chapter5_2.1.0.42.exe
</code>
    - Chapitre 6 :<code>
$ ls
</code><code>
libplayit2.sh
play-heroes-chronicles.sh
setup_heroes_chronicles_chapter6_2.1.0.42.exe
</code>
    - Chapitre 7 :<code>
$ ls
</code><code>
libplayit2.sh
play-heroes-chronicles.sh
setup_heroes_chronicles_chapter7_2.1.0.42.exe
</code>
    - Chapitre 8 :<code>
$ ls
</code><code>
libplayit2.sh
play-heroes-chronicles.sh
setup_heroes_chronicles_chapter8_2.1.0.42.exe
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-heroes-chronicles.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
