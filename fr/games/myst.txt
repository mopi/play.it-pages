====== Myst : L’Apogée ======
//version vendue sur GOG//

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-myst-masterpiece_gog-2.0.0.22.sh|play-myst-masterpiece_gog-2.0.0.22.sh]] (mis à jour le 5/04/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (mis à jour le 18/03/2018)
  * cibles :
    * setup_myst_masterpiece_2.0.0.22.exe (MD5 : e7a979dc6ca044eaec2984877ac032c5)
    * [[https://www.dotslashplay.it/ressources/myst/|scummvm-myst.tar.gz]]
  * dépendances :
    * fakeroot
    * innoextract
    * icoutils (optionnel)

[[https://www.dotslashplay.it/images/games/myst/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/myst/thumbnail.jpg?nocache }}]]

<note>Le jeu installé via ce script utilisera [[https://www.scummvm.org/|ScummVM]].</note>

==== Utilisation ====

  - Installez les dépendances des scripts :<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Placez dans un même répertoire les scripts et l’installeur :<code>
$ ls
</code><code>
play-anything.sh
play-myst-masterpiece_gog-2.0.0.22.sh
scummvm-myst.tar.gz
setup_myst_masterpiece_2.0.0.22.exe
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-myst-masterpiece_gog-2.0.0.22.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Utilisation avancée ====

Le script présenté ici peut prendre plusieurs options pour contrôler son comportement de manière plus fine. Suivez les liens pour avoir des détails sur leur utilisation.

  * [[:fr:commun:options-avancees#checksum|checksum]]
  * [[:fr:commun:options-avancees#compression|compression]]
  * [[:fr:commun:options-avancees#help|help]]
  * [[:fr:commun:options-avancees#prefix|prefix]]

==== Liens ====

  * [[https://fr.wikipedia.org/wiki/Myst|article Wikipédia]]
