====== Solar Flux ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-solar-flux.sh|play-solar-flux.sh]] (mis à jour le 13/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cible :
    * [[https://www.humblebundle.com/store/solar-flux|SolarFluxLinux.zip]] (MD5 : 9e2faa973a4affef9ddb1205f5f82019)
  * dépendances :
    * Arch Linux :
      * unzip
    * Debian :
      * fakeroot
      * unzip

[[https://www.dotslashplay.it/images/games/solar-flux/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/solar-flux/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S unzip
</code>
    - Debian :<code>
# apt-get install fakeroot unzip
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
libplayit2.sh
play-solar-flux.sh
SolarFluxLinux.zip
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-solar-flux.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
