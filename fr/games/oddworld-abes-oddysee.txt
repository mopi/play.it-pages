====== Oddworld: Abe’s Oddysee ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-oddworld-abes-oddysee.sh|play-oddworld-abes-oddysee.sh]] (mis à jour le 25/09/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cible :
    * [[https://www.gog.com/game/oddworld_abes_oddysee|setup_abes_oddysee_2.0.0.4.exe]] (MD5 : c22a44d208e524dc2760ea6ce57829d5)
  * dépendances :
    * Arch Linux :
      * icoutils
      * innoextract
    * Debian :
      * fakeroot
      * icoutils
      * innoextract

[[https://www.dotslashplay.it/images/games/oddworld-abes-oddysee/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/oddworld-abes-oddysee/thumbnail.jpg?nocache }}]]

<note>Le jeu installé via ce script utilisera [[https://www.winehq.org/|WINE]].</note>

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
$ yaourt icoutils innoextract
</code>
    - Debian :<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
libplayit2.sh
play-oddworld-abes-oddysee.sh
setup_abes_oddysee_2.0.0.4.exe
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-oddworld-abes-oddysee.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
