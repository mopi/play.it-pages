====== Brütal Legend ======

===== Description =====

[[https://www.dotslashplay.it/images/games/brutal-legend/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/brutal-legend/thumbnail.jpg?nocache }}]]

**Brütal Legend** : Brütal Legend est une aventure qui marie le combat d’action viscéral avec la liberté du monde ouvert, situé dans un univers rempli d’imitations de cover bands, de démons qui veulent asservir l’humanité et de titres Heavy Metal.

  * age : +17 ans
  * catégorie : aventure, action, stratégie
  * date de sortie : 2013

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-brutal-legend.sh|play-brutal-legend.sh]] (mis à jour le 8/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cibles :
    * GOG :
      * [[https://www.gog.com/game/brutal_legend|gog_brutal_legend_2.0.0.3.sh]] (MD5 : f5927fb8b3959c52e2117584475ffe49)
    * Humble Bundle :
      * [[https://www.humblebundle.com/store/brutal-legend|BrutalLegend-Linux-2013-06-15-setup.bin]] (MD5 : cbda6ae12aafe20a76f4d45367430d32)
  * dépendances :
    * Arch Linux :
      * unzip
    * Debian :
      * fakeroot
      * unzip

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S unzip
</code>
    - Debian :<code>
# apt-get install fakeroot unzip
</code>
  - Placez dans un même répertoire les scripts et l’archive :
    - GOG :<code>
$ ls
</code><code>
gog_brutal_legend_2.0.0.3.sh
libplayit2.sh
play-brutal-legend.sh
</code>
    - Humble Bundle :<code>
$ ls
</code><code>
BrutalLegend-Linux-2013-06-15-setup.bin
libplayit2.sh
play-brutal-legend.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-brutal-legend.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
