====== Blackwell 3: Blackwell Convergence ======

===== Description =====

**Blackwell Convergence** : Un nouveau film s’ouvre à des critiques élogieuses, malgré son histoire sanglante.\\ 
Un beau bureau du centre-ville reste inoccupé malgré son emplacement privilégié.\\
Un artiste du centre-ville se reproche d’avoir vendu, tandis qu'un investisseur de Wall Street se félicite d'un travail bien fait.\\
Une vie normale dans la grande ville ? Ou quelque chose de plus sinistre lie-t-il ces événements ?\\
Les connexions bizarres sont banales pour la famille Blackwell, mais jusqu'où remontent-t-elles ?\\
La médium Rosangela Blackwell et son guide spirituel Joey Mallone sont sur le point de le découvrir.

  * age : +12 ans
  * catégorie : action, ésoterisme, Point'n Click
  * url officielle : [[http://www.wadjeteyegames.com/games/blackwell-convergence/|]]
  * date de sortie : 2009

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-blackwell-3-blackwell-convergence.sh|play-blackwell-3-blackwell-convergence.sh]] (mis à jour le 14/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cible :
    * [[https://www.gog.com/game/blackwell_bundle|gog_blackwell_convergence_2.0.0.2.sh]] (MD5 : 784f9a8cf70213c938c801dadcd0b5e3)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/blackwell-3-blackwell-convergence/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/blackwell-3-blackwell-convergence/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_blackwell_convergence_2.0.0.2.sh
libplayit2.sh
play-blackwell-3-blackwell-convergence.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-blackwell-3-blackwell-convergence.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
