====== Windward ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-windward.sh|play-windward.sh]] (mis à jour le 25/09/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cibles :
    * GOG :
      * [[https://www.gog.com/game/windward|gog_windward_2.36.0.40.sh]] (MD5 : 6afbdcfda32a6315139080822c30396a)
    * Humble Bundle :
      * WindwardLinux_HB_1505248588.zip (MD5 : 9ea99157d13ae53905757f2fb3ab5b54)
  * dépendances :
    * Arch Linux :
      * GOG :
        * libarchive
      * Humble Bundle :
        * unzip
    * Debian :
      * GOG :
        * bsdtar
        * fakeroot
      * Humble Bundle :
        * fakeroot
        * unzip

[[https://www.dotslashplay.it/images/games/windward/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/windward/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :
      - GOG :<code>
# pacman -S libarchive
</code>
      - Humble Bundle :<code>
# pacman -S unzip
</code>
    - Debian :
      - GOG :<code>
# apt-get install bsdtar fakeroot
</code>
      - Humble Bundle :<code>
# apt-get install fakeroot unzip
</code>
  - Placez dans un même répertoire les scripts et l’archive :
    - GOG :<code>
$ ls
</code><code>
gog_windward_2.36.0.40.sh
libplayit2.sh
play-windward.sh
</code>
    - Humble Bundle :<code>
$ ls
</code><code>
libplayit2.sh
play-windward.sh
WindwardLinux_HB_1505248588.zip
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-windward.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
