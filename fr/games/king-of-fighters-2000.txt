====== The King of Fighters 2000 ======
//version vendue sur Humble Bundle//

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-king-of-fighters-2000_humble.sh|play-king-of-fighters-2000_humble.sh]] (mis à jour le 21/04/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (mis à jour le 18/03/2018)
  * cible :
    * KingOfFighters2000.sh (MD5 : 928064d176a666af534ce91cc2eeb591)
  * dépendances :
    * fakeroot
    * unzip

[[https://www.dotslashplay.it/images/games/king-of-fighters-2000/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/king-of-fighters-2000/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances du script :<code>
# apt-get install fakeroot unzip
</code>
  - Placez dans un même répertoire les scripts et l’installeur :<code>
$ ls
</code><code>
KingOfFighters2000.sh
play-anything.sh
play-king-of-fighters-2000_humble.sh
</code>
  - Lancez la construction du paquet depuis ce répertoire :<code>
$ sh ./play-king-of-fighters-2000_humble.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Utilisation avancée ====

Le script présenté ici peut prendre plusieurs options pour contrôler son comportement de manière plus fine. Suivez les liens pour avoir des détails sur leur utilisation.

  * [[:fr:commun:options-avancees#checksum|checksum]]
  * [[:fr:commun:options-avancees#compression|compression]]
  * [[:fr:commun:options-avancees#help|help]]
  * [[:fr:commun:options-avancees#prefix|prefix]]

==== Liens ====

  * [[https://fr.wikipedia.org/wiki/The_King_of_Fighters_2000|article Wikipédia]]
