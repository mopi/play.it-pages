====== Dex ======

===== Description =====

[[https://www.dotslashplay.it/images/games/dex/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/dex/thumbnail.jpg?nocache }}]]

**Dex** : Une mystérieuse organisation de grande envergure veut votre mort et vous devez traverser la ville futuriste de Harbor Prime en quête d’alliés inattendus pour faire tomber le système !

  * age : +17 ans
  * catégorie : RPG, Action, Science-Fiction
  * url officielle : [[http://en.dreadlocks.cz/games/dex/|]]
  * date de sortie : 2015

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-dex.sh|play-dex.sh]] (mis à jour le 8/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cible :
    * [[https://www.gog.com/game/dex|dex_en_6_0_0_0_build_5553_17130.sh]] (MD5 : 3d6f8797fab72dcb867c92bf5a84b4dd)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
dex_en_6_0_0_0_build_5553_17130.sh
libplayit2.sh
play-dex.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-dex.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
