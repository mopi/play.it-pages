====== Lure of the Temptress ======
//version vendue sur GOG//

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-lure-of-the-temptress_gog-2.0.0.6.sh|play-lure-of-the-temptress_gog-2.0.0.6.sh]] (mis à jour le 27/04/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (mis à jour le 18/03/2018)
  * cible :
    * version anglaise :
      * gog_lure_of_the_temptress_2.0.0.6.sh (MD5 : 86d110cf60accee567af61e22657a14f)
    * version française :
      * gog_lure_of_the_temptress_french_2.0.0.6.sh (MD5 : d3f454f2d328b5ac91874e79c0b4b0ca)
  * dépendances :
    * fakeroot
    * unzip

[[https://www.dotslashplay.it/images/games/lure-of-the-temptress/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/lure-of-the-temptress/thumbnail.jpg?nocache }}]]

<note>Le jeu installé via ces scripts utilisera [[https://www.scummvm.org/|ScummVM]].</note>

==== Utilisation ====

  - Installez les dépendances des scripts :<code>
# apt-get install fakeroot unzip
</code>
  - Placez dans un même répertoire les scripts et l’installeur :<code>
$ ls
</code><code>
gog_lure_of_the_temptress_2.0.0.6.sh
play-anything.sh
play-lure-of-the-temptress_gog-2.0.0.6.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-lure-of-the-temptress_gog-2.0.0.6.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Utilisation avancée ====

Le script présenté ici peut prendre plusieurs options pour contrôler son comportement de manière plus fine. Suivez les liens pour avoir des détails sur leur utilisation.

  * [[:fr:commun:options-avancees#checksum|checksum]]
  * [[:fr:commun:options-avancees#compression|compression]]
  * [[:fr:commun:options-avancees#help|help]]
  * [[:fr:commun:options-avancees#prefix|prefix]]
