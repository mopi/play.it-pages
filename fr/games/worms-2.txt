====== Worms 2 ======
//version vendue sur GOG//

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (mis à jour le 18/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-worms-2_gog-2.0.0.23.sh|play-worms-2_gog-2.0.0.23.sh]]
  * cible :
    * setup_worms2_2.0.0.23.exe
  * cible optionnelle :
    * [[https://www.dotslashplay.it/ressources/worms-2/|worms-2_wmv-sync.7z]]
  * dépendances :
    * fakeroot
    * icoutils
    * innoextract

[[https://www.dotslashplay.it/images/games/worms-2/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/worms-2/thumbnail.jpg?nocache }}]]

<note>Le jeu installé via ce script utilisera [[https://www.winehq.org/|WINE]].</note>

==== Utilisation ====

  - Installez les dépendances du script :<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Téléchargez et placez dans un même répertoire les scripts et l’installeur :<code>
$ ls
</code><code>
play-anything.sh
play-worms-2_gog-2.0.0.23.sh
setup_worms2_2.0.0.23.exe
</code>
  - Lancez la construction du paquet depuis ce répertoire :<code>
$ sh ./play-worms-2_gog-2.0.0.23.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu.

==== Utilisation avancée ====

Le script présenté ici peut prendre plusieurs options pour contrôler son comportement de manière plus fine. Suivez les liens pour avoir des détails sur leur utilisation.

  * [[:fr:commun:options-avancees#checksum|checksum]]
  * [[:fr:commun:options-avancees#compression|compression]]
  * [[:fr:commun:options-avancees#help|help]]
  * [[:fr:commun:options-avancees#prefix|prefix]]

==== Configurer le préfixe WINE ====

Vous pouvez accéder à la fenêtre de configuration du préfixe WINE dédié au jeu en lançant la commande suivante :
<code>
worms2-winecfg
</code>
Toute modification effectuée via cette fenêtre sera spécifique à ce jeu, sans influence pour vos autres jeux WINE, qu’ils aient été installés via un script ./play.it ou un appel direct à WINE.

==== Lancer le jeu dans une fenêtre ====

La méthode suivante va vous permettre de lancer le jeu en mode fenêtré.
  - Ouvrez la fenêtre de configuration du préfixe WINE, via la commande donnée plus haut ;
  - Dans la fenêtre qui s’affiche, allez sur l’onglet "Affichage" ;
  - Cochez la case "Émuler un bureau virtuel", et dans les champs "Taille du bureau" renseignez la résolution qu’utilise votre bureau ;
  - Fermez la fenêtre avec "OK".
C’est fait, Worms 2 se lancera maintenant dans un fenêtre adaptée à la résolution qu’il utilise. Vous pouvez à tout moment revenir à un mode plein écran en retournant dans la fenêtre de configuration de WINE et en décochant la case "Émuler un bureau virtuel".

==== Liens ====

  * [[https://fr.wikipedia.org/wiki/Worms_2|Worms 2 sur Wikipédia]]
