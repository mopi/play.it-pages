====== Deus Ex ======

===== Description =====

[[https://www.dotslashplay.it/images/games/deus-ex/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/deus-ex/thumbnail.jpg?nocache }}]]

**Deus Ex** : Dans l’avenir dystopien de l'année 2052, la société se transforme lentement en chaos.\\
Un virus mortel, la "Mort Grise", ravage le monde. Le seul vaccin, "Ambrosia", est tellement rare qu’il n’est disponible que pour ceux qui sont suffisamment riches et jugés "vitaux pour l'ordre social".\\
Sans espoir d’un remède pour les gens ordinaires, des émeutes se produisent dans le monde entier et un certain nombre d’organisations terroristes se sont formées. \\
En réponse, les Nations Unies ont créé la Coalition antiterroriste des Nations Unies (UNATCO). \\
Vous assumez le rôle de JC Denton, un agent de l'UNATCO renforcé par les nanotechnologies…

  * age : +16 ans
  * catégorie : RPG, FPS, Infiltration
  * date de sortie : 2000

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-deus-ex.sh|play-deus-ex.sh]] (mis à jour le 14/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cible :
    * [[https://www.gog.com/game/deus_ex|setup_deus_ex_goty_1.112fm(revision_1.3.1)_(17719).exe]] (MD5 : 92e9e6a33642f9e6c41cb24055df9b3c)
  * dépendances :
    * Arch Linux :
      * icoutils
      * innoextract
    * Debian :
      * fakeroot
      * icoutils
      * innoextract

<note>Le jeu installé via ce script utilisera [[https://www.winehq.org/|WINE]].</note>

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
$ yaourt icoutils innoextract
</code>
    - Debian :<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
libplayit2.sh
play-deus-ex.sh
setup_deus_ex_goty_1.112fm(revision_1.3.1)_(17719).exe
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-deus-ex.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
