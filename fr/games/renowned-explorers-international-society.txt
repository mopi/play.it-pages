====== Renowned Explorers: International Society ======

=== Renowned Explorers: International Society ===

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-renowned-explorers-international-society.sh|play-renowned-explorers-international-society.sh]] (mis à jour le 28/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cible :
    * [[https://www.gog.com/game/renowned_explorers|renowned_explorers_international_society_en_466_15616.sh]] (MD5 : fbad4b4d361a0e7d29b9781e3c5a5e85)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

=== Renowned Explorers: More To Explore ===

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-renowned-explorers-more-to-explore.sh|play-renowned-explorers-more-to-explore.sh]] (mis à jour le 28/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cible :
    * [[https://www.gog.com/game/renowned_explorers_more_to_explore|renowned_explorers_more_to_explore_dlc_en_466_15616.sh]] (MD5 : c99ca440cb312b90052939db49aeef03)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/renowned-explorers-international-society/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/renowned-explorers-international-society/thumbnail.jpg?nocache }}]]

==== Utilisation (Renowned Explorers: International Society) ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
libplayit2.sh
play-renowned-explorers-international-society.sh
renowned_explorers_international_society_en_466_15616.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-renowned-explorers-international-society.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Utilisation (Renowned Explorers: More To Explore) ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
libplayit2.sh
play-renowned-explorers-more-to-explore.sh
renowned_explorers_more_to_explore_dlc_en_466_15616.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-renowned-explorers-more-to-explore.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
