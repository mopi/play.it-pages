====== SteamWorld Dig ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-steamworld-dig.sh|play-steamworld-dig.sh]] (mis à jour le 14/10/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cibles :
    * GOG :
      * [[https://www.gog.com/game/steamworld_dig|gog_steamworld_dig_2.0.0.7.sh]] (MD5 : 2f2ed68e00f151ff3c4d0092d8d6b15b)
    * Humble Bundle :
      * [[https://www.humblebundle.com/store/steamworld-dig|SteamWorldDig_linux_1393468453.tar.gz]] (MD5 : de6ff6273c4e397413d852472d51e788)
  * dépendances :
    * GOG :
      * Arch Linux :
        * libarchive
      * Debian :
        * bsdtar
        * fakeroot
    * Humble Bundle :
      * Debian :
        * fakeroot

[[https://www.dotslashplay.it/images/games/steamworld-dig/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/steamworld-dig/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - GOG :
      - Arch Linux :<code>
# pacman -S libarchive
</code>
      - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
    - Humble Bundle :
      - Debian :<code>
# apt-get install fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :
    - GOG :<code>
$ ls
</code><code>
gog_steamworld_dig_2.0.0.7.sh
libplayit2.sh
play-steamworld-dig.sh
</code>
    - Humble Bundle :<code>
$ ls
</code><code>
libplayit2.sh
play-steamworld-dig.sh
SteamWorldDig_linux_1393468453.tar.gz
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-steamworld-dig.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
