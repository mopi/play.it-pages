====== Torment: Tides of Numenera ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-torment-tides-of-numenera.sh|play-torment-tides-of-numenera.sh]] (mis à jour le 24/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cible :
    * [[https://www.gog.com/game/torment_tides_of_numenera|gog_torment_tides_of_numenera_2.2.0.3.sh]] (MD5 : e63c62f4453d0fef40a1c930db9574e8)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/torment-tides-of-numenera/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/torment-tides-of-numenera/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_torment_tides_of_numenera_2.2.0.3.sh
libplayit2.sh
play-torment-tides-of-numenera.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-torment-tides-of-numenera.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
