====== Akalabeth ======

===== Description =====

[[https://www.dotslashplay.it/images/games/akalabeth/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/akalabeth/thumbnail.jpg?nocache }}]]

**Akalabeth**: le joueur se voit confier par un certain Lord British des quêtes en vue de tuer une suite de dix monstres de plus en plus difficiles à vaincre.

  * catégorie : RPG, aventure
  * date de sortie : 1979 (Apple II), 1998 (DOS)

===== Informations =====

//version vendue sur GOG//

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-akalabeth.sh|play-akalabeth.sh]] (mis à jour le 30/10/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (mis à jour le 18/03/2018)
  * cibles :
    * version originale :
      * gog_akalabeth_world_of_doom_2.0.0.3.sh (MD5 : 11a770db592af2ac463e6cdc453b555b)
    * version de 1998 :
      * akalabeth_1998_linux.zip (MD5 : 9c549339b300c3bfe73f8430d8fc74af)
  * dépendances :
    * fakeroot
    * unzip
    * icoutils (optionnel)

<note>Le jeu installé via ce script utilisera [[https://www.dosbox.com/|DOSBox]].</note>

==== Utilisation ====

  - Installez les dépendances des scripts :<code>
# apt-get install unzip fakeroot icoutils
</code>
  - Placez dans un même répertoire les scripts et l’installeur :<code>
$ ls
</code><code>
gog_akalabeth_world_of_doom_2.0.0.3.sh
play-anything.sh
play-akalabeth.sh
</code>
  - Lancez la construction du paquet depuis ce répertoire :<code>
$ sh ./play-akalabeth.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Utilisation avancée ====

Le script présenté ici peut prendre plusieurs options pour contrôler son comportement de manière plus fine. Suivez les liens pour avoir des détails sur leur utilisation.

  * [[:fr:commun:options-avancees#checksum|checksum]]
  * [[:fr:commun:options-avancees#compression|compression]]
  * [[:fr:commun:options-avancees#help|help]]
  * [[:fr:commun:options-avancees#prefix|prefix]]

==== Liens ====

  * [[https://fr.wikipedia.org/wiki/Akalabeth|article Wikipédia]]
