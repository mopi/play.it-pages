====== Gnomoria ======

===== Description =====

[[https://www.dotslashplay.it/images/games/gnomoria/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/gnomoria/thumbnail.jpg?nocache }}]]

**Gnomoria** Gnomoria** est un jeu de gestion de village où vous aidez à diriger un petit groupe de gnomes, qui se sont mis à leur compte, pour prospérer dans un royaume animé !\\
Tout ce que vous voyez peut être détruit et reconstruit ailleurs. Faites de l’artisanat, construisez des structures, installez des pièges et creusez profondément dans le sous-sol à la recherche de ressources précieuses pour aider vos gnomes à survivre sur les terres difficiles.\\
Construisez votre royaume et accumulez des richesses pour attirer des nomades errants à votre cause, mais méfiez-vous aussi d'attirer des ennemis !

  * catégorie : stratégie, City-Builder
  * date de sortie : 2016

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-gnomoria.sh|play-gnomoria.sh]] (mis à jour le 24/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cible :
    * [[https://www.gog.com/game/gnomoria|gog_gnomoria_2.0.0.1.sh]] (MD5 : 3d0a9ed4fb45ff133b5a7410a2114455)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_gnomoria_2.0.0.1.sh
libplayit2.sh
play-gnomoria.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-gnomoria.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Liens ====

  * [[http://gnomoria.com/|site officiel]] (en anglais)
