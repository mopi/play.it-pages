====== Knights of Pen and Paper ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-knights-of-pen-and-paper.sh|play-knights-of-pen-and-paper.sh]] (mis à jour le 30/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cible :
    * [[https://www.gog.com/game/knights_of_pen_and_paper_1_deluxier_edition|gog_knights_of_pen_and_paper_1_edition_2.0.0.1.sh]] (MD5 : 1f387b78bfe426b9396715fbfe3499b9)
    * [[https://www.gog.com/game/knights_of_pen_and_paper_1_deluxier_edition|gog_knights_of_pen_and_paper_1_deluxier_edition_upgrade_2.0.0.1.sh]] (MD5 : b3033693afd93cc885883aede7ede4b0)
  * dépendances :
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/knights-of-pen-and-paper/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/knights-of-pen-and-paper/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_knights_of_pen_and_paper_1_deluxier_edition_upgrade_2.0.0.1.sh
gog_knights_of_pen_and_paper_1_edition_2.0.0.1.sh
libplayit2.sh
play-knights-of-pen-and-paper.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-knights-of-pen-and-paper.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
