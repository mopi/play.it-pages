====== Trine Enchanted Edition ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-trine.sh|play-trine.sh]] (mis à jour le 30/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cibles :
    * [[https://www.gog.com/game/trine_enchanted_edition|gog_trine_enchanted_edition_2.0.0.2.sh]] (MD5 : 0e8d2338b568222b28cf3c31059b4960)
    * [[https://www.dotslashplay.it/ressources/libpng/|libpng_1.2_32-bit.tar.gz]] (MD5 : 15156525b3c6040571f320514a0caa80)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/trine/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/trine/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_trine_enchanted_edition_2.0.0.2.sh
libplayit2.sh
libpng_1.2_32-bit.tar.gz
play-trine.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-trine.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
