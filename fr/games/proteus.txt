====== Proteus ======
//version vendue sur Humble Bundle//

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-proteus_humble-2014-05-16.sh|play-proteus_humble-2014-05-16.sh]] (mis à jour le 5/06/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (mis à jour le 18/03/2018)
  * cible :
    * proteus-05162014-bin (MD5 : 8a5911751382bcfb91483f52f781e283)
  * dépendances :
    * fakeroot
    * unzip

[[https://www.dotslashplay.it/images/games/proteus/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/proteus/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :<code>
# apt-get install fakeroot unzip
</code>
  - Placez dans un même répertoire les scripts et l’installeur :<code>
$ ls
</code><code>
play-anything.sh
play-proteus_humble-2014-05-16.sh
proteus-05162014-bin
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-proteus_humble-2014-05-16.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Utilisation avancée ====

Le script présenté ici peut prendre plusieurs options pour contrôler son comportement de manière plus fine. Suivez les liens pour avoir des détails sur leur utilisation.

  * [[:fr:commun:options-avancees#checksum|checksum]]
  * [[:fr:commun:options-avancees#compression|compression]]
  * [[:fr:commun:options-avancees#help|help]]
  * [[:fr:commun:options-avancees#prefix|prefix]]

==== Liens ====

  * [[https://fr.wikipedia.org/wiki/Proteus_(jeu_vidéo)|article Wikipédia]]
