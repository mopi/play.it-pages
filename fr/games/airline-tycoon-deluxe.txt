====== Airline Tycoon Deluxe ======

===== Description =====

**Airline Tycoon Deluxe** est une simulation économique délirante en temps réel qui vous placera aux commandes d’une compagnie aérienne.

  * age : tous
  * catégorie : simulation
  * date de sortie : 1998

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-airline-tycoon-deluxe.sh|play-airline-tycoon-deluxe.sh]] (mis à jour le 28/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cible :
    * [[https://www.gog.com/game/airline_tycoon_deluxe|gog_airline_tycoon_deluxe_2.0.0.9.sh]] (MD5 : dc8b78da150bd3b2089120cc2d24353c)
  * dépendances :
    * Arch Linux :
      * unzip
    * Debian :
      * fakeroot
      * unzip

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S unzip
</code>
    - Debian :<code>
# apt-get install fakeroot unzip
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_airline_tycoon_deluxe_2.0.0.9.sh
libplayit2.sh
play-airline-tycoon-deluxe.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-airline-tycoon-deluxe.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
