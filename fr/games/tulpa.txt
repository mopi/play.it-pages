====== Tulpa ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-tulpa.sh|play-tulpa.sh]] (mis à jour le 15/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cible :
    * [[https://www.humblebundle.com/store/tulpa|Tulpa_Linux_1423847478.zip]] (MD5 : 3e01614c5c1c562aaf423689a1f51df9)
  * dépendances :
    * Arch Linux :
      * unzip
    * Debian :
      * fakeroot
      * unzip

[[https://www.dotslashplay.it/images/games/tulpa/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/tulpa/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S unzip
</code>
    - Debian :<code>
# apt-get install fakeroot unzip
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
libplayit2.sh
play-tulpa.sh
Tulpa_Linux_1423847478.zip
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-tulpa.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
