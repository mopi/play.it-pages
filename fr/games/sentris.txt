====== Sentris ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-sentris.sh|play-sentris.sh]] (mis à jour le 24/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cible :
    * [[https://www.humblebundle.com/store/sentris|Sentris_1.02_Linux.zip]] (MD5 : b6ca5f3447a223efba73ae9138b16a45)
  * dépendances :
    * Arch Linux :
      * unzip
    * Debian :
      * fakeroot
      * unzip

[[https://www.dotslashplay.it/images/games/sentris/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/sentris/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S unzip
</code>
    - Debian :<code>
# apt-get install fakeroot unzip
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
Sentris_1.02_Linux.zip
libplayit2.sh
play-sentris.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-sentris.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
