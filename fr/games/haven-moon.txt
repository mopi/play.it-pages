====== Haven Moon ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-haven-moon.sh|play-haven-moon.sh]] (mis à jour le 24/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cible :
    * [[https://www.humblebundle.com/store/haven-moon|HavenMoon_LINUX.zip]] (MD5 : 2698639eaad59d4d01c10694bf63188e)
  * dépendances :
    * Debian :
      * fakeroot

[[https://www.dotslashplay.it/images/games/haven-moon/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/haven-moon/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Debian :<code>
# apt-get install fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
HavenMoon_LINUX.zip
libplayit2.sh
play-haven-moon.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-haven-moon.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
