====== Ascendant ======

===== Description =====

[[https://www.dotslashplay.it/images/games/ascendant/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/ascendant/thumbnail.jpg?nocache }}]]

**Ascendant** : vous incarnez un guerrier armé d’un arc et d’une lame ornée d’un cristal.

  * catégorie : beat’m all
  * date de sortie : 2014

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-ascendant.sh|play-ascendant.sh]] (mis à jour le 24/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cible :
    * [[https://www.gog.com/game/ascendant|gog_ascendant_2.2.0.7.sh]] (MD5 : 8cdcd59a2f8363b7237e9cbe2675adda)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_ascendant_2.2.0.7.sh
libplayit2.sh
play-ascendant.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-ascendant.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Liens ====

  * [[http://hapagames.com/game.html|site officiel]] (en anglais)
