====== A Virus Named Tom ======

===== Description =====

[[https://www.dotslashplay.it/images/games/a-virus-named-tom/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/a-virus-named-tom/thumbnail.jpg?nocache }}]]

**A Virus Named Tom**: vous prenez le contrôle de TOM, (produit du brillant esprit de Dr. X, un scientifique fou viré d’une gigantesque corporation), un virus créé pour un seul but : la revanche.

  * age : tous
  * catégorie : puzzle, réflexion
  * url officielle : [[http://www.avirusnamedtom.com/|]]
  * date de sortie : 2012

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-a-virus-named-tom.sh|play-a-virus-named-tom.sh]] (mis à jour le 14/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 18/03/2018)
  * cible :
    * [[https://www.humblebundle.com/store/a-virus-named-tom|avnt-09172013-bin]] (MD5 : 85d11d3f05ad966a06a7e2f77e2fee45)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
avnt-09172013-bin
libplayit2.sh
play-a-virus-named-tom.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-a-virus-named-tom.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
